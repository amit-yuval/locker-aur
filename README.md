# Locker AUR Installation `PKGBUILD`

## Installation

Clone the repo, and in it run:
```bash
makepkg
sudo pacman -U locker-{VER}-{PKGREL}-x86_64.pkg.tar.xz
```

#### Please Note:

 * replace `{VER}` with the package `$pkgver`
 * replace `{PKGREL}` with the package `$pkgrel`, usually 0